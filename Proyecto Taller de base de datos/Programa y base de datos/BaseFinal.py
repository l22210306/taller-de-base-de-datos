import tkinter as tk
from tkinter import messagebox, ttk
import pyodbc

# Conexión a la base de datos
def conectar_bd():
    try:
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};'
                              'SERVER=DESKTOP-2QUJGJR\\SQLEXPRESS;'
                              'DATABASE=Certificados;'
                              'Trusted_Connection=yes;')
        return conn
    except pyodbc.Error as e:
        messagebox.showerror("Error de conexión", f"No se pudo conectar a la base de datos: {str(e)}")
        return None

# Función para centrar la ventana
def centrar_ventana(ventana):
    ventana.update_idletasks()
    ancho = ventana.winfo_width()
    alto = ventana.winfo_height()
    x = (ventana.winfo_screenwidth() // 2) - (ancho // 2)
    y = (ventana.winfo_screenheight() // 2) - (alto // 2)
    ventana.geometry(f'{ancho}x{alto}+{x}+{y}')

# Función para consultar y llenar el Treeview de cualquier tabla
def consultar_tabla(treeview, tabla, columnas):
    conn = conectar_bd()
    if conn:
        try:
            cursor = conn.cursor()
            cursor.execute(f"SELECT * FROM {tabla}")
            resultados = cursor.fetchall()
            
            # Limpiar el Treeview antes de agregar nuevos datos
            treeview.delete(*treeview.get_children())
            treeview["columns"] = columnas
            
            # Configurar las columnas del Treeview
            for col in columnas:
                treeview.heading(col, text=col)
                treeview.column(col, width=150)
            
            # Agregar cada fila de resultados al Treeview
            for row in resultados:
                # Convertir cada campo a una cadena para asegurarse de que se muestre correctamente
                values = [str(item) for item in row]
                treeview.insert('', 'end', values=values)
        
        except pyodbc.Error as e:
            messagebox.showerror("Error", f"Error al consultar la tabla: {str(e)}")
        finally:
            cursor.close()
            conn.close()

# Función para eliminar una fila de la base de datos
def eliminar_fila(tabla, columna_id, valor_id):
    conn = conectar_bd()
    if conn:
        try:
            cursor = conn.cursor()
            query = f"DELETE FROM {tabla} WHERE {columna_id} = ?"
            cursor.execute(query, (valor_id,))
            conn.commit()
            messagebox.showinfo("Éxito", "Registro eliminado correctamente")
        except pyodbc.Error as e:
            messagebox.showerror("Error", f"Error al eliminar el registro: {str(e)}")
        finally:
            cursor.close()
            conn.close()

# Variable para almacenar la ventana secundaria actual
ventana_actual = None

# Función para insertar datos en una tabla
def insertar_datos(tabla, columnas, valores):
    conn = conectar_bd()
    if conn:
        try:
            cursor = conn.cursor()
            columnas_str = ', '.join(columnas)
            valores_str = ', '.join(['?' for _ in valores])
            query = f"INSERT INTO {tabla} ({columnas_str}) VALUES ({valores_str})"
            cursor.execute(query, valores)
            conn.commit()
            messagebox.showinfo("Éxito", "Datos insertados correctamente")
        except pyodbc.Error as e:
            messagebox.showerror("Error", f"Error al insertar datos: {str(e)}")
        finally:
            cursor.close()
            conn.close()

# Crear ventanas para cada tabla
def crear_ventana_tabla(titulo, tabla, columnas):
    global ventana_actual
    if ventana_actual is not None:
        ventana_actual.destroy()

    nueva_ventana = tk.Toplevel()
    ventana_actual = nueva_ventana
    nueva_ventana.title(titulo)
    nueva_ventana.geometry("800x600")
    centrar_ventana(nueva_ventana)

    etiqueta = tk.Label(nueva_ventana, text=titulo)
    etiqueta.pack(pady=10)

    treeview = ttk.Treeview(nueva_ventana, show='headings')
    treeview.pack(pady=10, padx=10, fill='both', expand=True)

    frame_form = tk.Frame(nueva_ventana)
    frame_form.pack(pady=10)

    entries = {}
    for i, col in enumerate(columnas):
        label = tk.Label(frame_form, text=col)
        label.grid(row=i, column=0, padx=5, pady=5)
        entry = tk.Entry(frame_form)
        entry.grid(row=i, column=1, padx=5, pady=5)
        entries[col] = entry

    def obtener_valores():
        return [entries[col].get() for col in columnas]

    boton_insertar = tk.Button(nueva_ventana, text="Insertar", command=lambda: insertar_datos(tabla, columnas, obtener_valores()))
    boton_insertar.pack(side=tk.LEFT, padx=10, pady=10)

    boton_consultar = tk.Button(nueva_ventana, text="Consultar", command=lambda: consultar_tabla(treeview, tabla, columnas))
    boton_consultar.pack(side=tk.LEFT, padx=10, pady=10)

    def borrar_fila_seleccionada():
        selected_item = treeview.selection()[0]
        valores = treeview.item(selected_item, 'values')
        valor_id = valores[0]  # Asumiendo que la primera columna es el ID
        treeview.delete(selected_item)
        eliminar_fila(tabla, columnas[0], valor_id)

    boton_borrar = tk.Button(nueva_ventana, text="Borrar", command=borrar_fila_seleccionada)
    boton_borrar.pack(side=tk.LEFT, padx=10, pady=10)

    boton_salir = tk.Button(nueva_ventana, text="Salir al menú", command=nueva_ventana.destroy)
    boton_salir.pack(side=tk.RIGHT, padx=10, pady=10)

ventana = tk.Tk()
ventana.title("Certificaciones y proyectos")
ventana.geometry("700x500")
centrar_ventana(ventana)

ventana.grid_columnconfigure((0, 1, 2), weight=1)
ventana.grid_rowconfigure(tuple(range(1, 7)), weight=1)

etiqueta = tk.Label(ventana, text="Menú inicio")
etiqueta.grid(row=0, column=1, pady=10)

# Crear los botones y acomodarlos en una cuadrícula
boton1 = tk.Button(ventana, text="Empleados", command=lambda: crear_ventana_tabla("Empleados", "Empleados", ["ID", "Nombre", "Departamento", "Puesto", "FechaContratacion", "Email", "Telefono"]))
boton1.grid(row=1, column=0, padx=5, pady=5, sticky="nsew")

boton2 = tk.Button(ventana, text="Certificaciones", command=lambda: crear_ventana_tabla("Certificaciones", "Certificaciones", ["ID", "Nombre", "Descripcion", "FechaVencimiento"]))
boton2.grid(row=1, column=1, padx=5, pady=5, sticky="nsew")

boton3 = tk.Button(ventana, text="Cumplimientos Legales", command=lambda: crear_ventana_tabla("Cumplimientos Legales", "CumplimientosLegales", ["ID", "Nombre", "Descripcion", "FechaVencimiento"]))
boton3.grid(row=1, column=2, padx=5, pady=5, sticky="nsew")

boton4 = tk.Button(ventana, text="Empleados y Certificaciones", command=lambda: crear_ventana_tabla("Empleados_Certificaciones", "Empleados_Certificaciones", ["EmpleadoID", "CertificacionID", "FechaObtencion"]))
boton4.grid(row=2, column=0, padx=5, pady=5, sticky="nsew")

boton5 = tk.Button(ventana, text="Empleados y Cumplimientos Legales", command=lambda: crear_ventana_tabla("Empleados_CumplimientosLegales", "Empleados_CumplimientosLegales", ["EmpleadoID", "CumplimientoLegalID", "FechaCumplimiento"]))
boton5.grid(row=2, column=1, padx=5, pady=5, sticky="nsew")

boton6 = tk.Button(ventana, text="Actualizaciones de Certificaciones", command=lambda: crear_ventana_tabla("ActualizacionesCertificaciones", "ActualizacionesCertificaciones", ["ID", "EmpleadoID", "CertificacionID", "FechaActualizacion", "Observaciones"]))
boton6.grid(row=2, column=2, padx=5, pady=5, sticky="nsew")

boton7 = tk.Button(ventana, text="Actualizaciones de Cumplimientos Legales", command=lambda: crear_ventana_tabla("ActualizacionesCumplimientosLegales", "ActualizacionesCumplimientosLegales", ["ID", "EmpleadoID", "CumplimientoLegalID", "FechaActualizacion", "Observaciones"]))
boton7.grid(row=3, column=0, padx=5, pady=5, sticky="nsew")

boton8 = tk.Button(ventana, text="Capacitaciones", command=lambda: crear_ventana_tabla("Capacitaciones", "Capacitaciones", ["ID", "Nombre", "Descripcion", "FechaInicio", "FechaFin"]))
boton8.grid(row=3, column=1, padx=5, pady=5, sticky="nsew")

boton9 = tk.Button(ventana, text="Empleados y Capacitaciones", command=lambda: crear_ventana_tabla("Empleados_Capacitaciones", "Empleados_Capacitaciones", ["EmpleadoID", "CapacitacionID", "FechaParticipacion"]))
boton9.grid(row=3, column=2, padx=5, pady=5, sticky="nsew")

boton10 = tk.Button(ventana, text="Empleados Entrenamientos", command=lambda: crear_ventana_tabla("Empleados_Entrenamientos", "Empleados_Entrenamientos", ["EmpleadoID", "EntrenamientoID", "FechaRealizacion"]))
boton10.grid(row=4, column=0, padx=5, pady=5, sticky="nsew")

boton11 = tk.Button(ventana, text="Incidentes y Seguridad", command=lambda: crear_ventana_tabla("IncidentesSeguridad", "IncidentesSeguridad", ["ID", "FechaIncidente", "Descripcion", "EmpleadoID"]))
boton11.grid(row=4, column=1, padx=5, pady=5, sticky="nsew")

boton12 = tk.Button(ventana, text="Evaluaciones de Desempeno", command=lambda: crear_ventana_tabla("EvaluacionesDesempeno", "EvaluacionesDesempeno", ["ID", "EmpleadoID", "FechaEvaluacion", "Descripcion"]))
boton12.grid(row=4, column=2, padx=5, pady=5, sticky="nsew")

boton13 = tk.Button(ventana, text="Auditorias Internas", command=lambda: crear_ventana_tabla("AuditoriasInternas", "AuditoriasInternas", ["ID", "FechaAuditoria", "Descripcion", "ResponsableID"]))
boton13.grid(row=5, column=0, padx=5, pady=5, sticky="nsew")

boton14 = tk.Button(ventana, text="Hallazgos de Auditorias", command=lambda: crear_ventana_tabla("HallazgosAuditorias", "HallazgosAuditorias", ["ID", "AuditoriaID", "Descripcion", "AccionCorrectiva", "FechaResolucion"]))
boton14.grid(row=5, column=1, padx=5, pady=5, sticky="nsew")

boton15 = tk.Button(ventana, text="Permisos y Licencias", command=lambda: crear_ventana_tabla("PermisosLicencias", "PermisosLicencias", ["ID", "Tipo", "Descripcion", "FechaEmision", "FechaExpiracion"]))
boton15.grid(row=5, column=2, padx=5, pady=5, sticky="nsew")

boton16 = tk.Button(ventana, text="Empleados y Permisos/Licencias", command=lambda: crear_ventana_tabla("Empleados_PermisosLicencias", "Empleados_PermisosLicencias", ["EmpleadoID", "PermisoLicenciaID"]))
boton16.grid(row=6, column=0, padx=5, pady=5, sticky="nsew")

boton_salir = tk.Button(ventana, text="Salir", command=ventana.quit)
boton_salir.grid(row=6, column=1, padx=10, pady=5, sticky="nsew")

ventana.mainloop()
